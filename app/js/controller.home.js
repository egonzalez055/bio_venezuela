'use strict';

index_controller.controller( 'HomeController' , 
  [ 
    '$state' ,
    'Texts'  ,
    function( $state , Texts )
      {
        var vm = this;

        vm.x        = true;
        vm.language = Texts.español;
        vm.showModal = false;

        vm.image_map = "img/maps_venezuela/19.png";;

        vm.array_of_point = 
        [
          {"id":1,"type":"Monumento Natural","name":"Karaurín","state":"Bolívar","latitude":5.1757826,"longitude":"-60.9463147","area":250000.00,"date":"2-11-1990","link":"4250-E \n18/01/1991","date":"1233\n02/11/1990","in_charge_of":"INPARQUES","declaration":""},
          {"id":2,"type":"Monumento Natural","name":"Yuruaní","state":"Bolívar","latitude":5.1757826,"longitude":"-60.9463147","area":250000.00,"date":"2-11-1990","link":"4250-E \n18/01/1991","date":"1233\n02/11/1990","in_charge_of":"INPARQUES","declaration":""},
          {"id":3,"type":"Monumento Natural","name":"Kukenán","state":"Bolívar","latitude":5.1757211,"longitude":"-60.9463155","area":250000.00,"date":"2-11-1990","link":"4250-E \n18/01/1991","date":"1233\n02/11/1990","in_charge_of":"INPARQUES","declaration":""},
          {"id":4,"type":"Monumento Natural","name":"Uei","state":"Bolívar","latitude":5.016944,"longitude":"-60.6174667","area":250000.00,"date":"2-11-1990","link":"4250-E \n18/01/1991","date":"1233\n02/11/1990","in_charge_of":"INPARQUES","declaration":""},
          {"id":5,"type":"Monumento Natural","name":"Wadakapiapué","state":"Bolívar","latitude":5.316389,"longitude":"-60.9252447","area":250000.00,"date":"2-11-1990","link":"4250-E \n18/01/1991","date":"1233\n02/11/1990","in_charge_of":"INPARQUES","declaration":""},
          {"id":6,"type":"Monumento Natural","name":"Llu","state":"Bolívar","latitude":5.405,"longitude":"-61.0077447","area":250000.00,"date":"2-11-1990","link":"4250-E \n18/01/1991","date":"1233\n02/11/1990","in_charge_of":"INPARQUES","declaration":""},
          {"id":7,"type":"Monumento Natural","name":"Cerro Guaiquinima","state":"Bolívar","latitude":5.8094444,"longitude":"-63.748022","area":170000.00,"date":"2-11-1990","link":"4250-E \n18/01/1991","date":"1233\n02/11/1990","in_charge_of":"INPARQUES","declaration":""},
          {"id":8,"type":"Monumento Natural","name":"Cerro Ichum y Guanacoco","state":"Bolívar","latitude":4,"longitude":"-63.966667","area":90000.00,"date":"2-11-1990","link":"4250-E\n18/01/1991","date":"1.233\n02/11/1990","in_charge_of":"INPARQUES","declaration":""},
          {"id":9,"type":"Monumento Natural","name":"Cerro Venamo","state":"Bolívar","latitude":5.95,"longitude":"-61.385522","area":7500.00,"date":"2-11-1990","link":"4250-E\n18/01/1991","date":"1233\n02/11/1990","in_charge_of":"INPARQUES","declaration":""},
          {"id":10,"type":"Monumento Natural","name":"Sierra Maigualida","state":"Bolívar","latitude":4.833333,"longitude":"-65.666667","area":260000.00,"date":"2-11-1990","link":"4250-E \n18/01/1991","date":"1233\n02/11/1990","in_charge_of":"INPARQUES","declaration":""},
          {"id":11,"type":"Monumento Natural","name":"Sierra Marutani","state":"Bolívar","latitude":3.7666667,"longitude":"-63.0521887","area":267500.00,"date":"2-11-1990","link":"4250-E\n18/01/1991","date":"1233\n02/11/1990","in_charge_of":"INPARQUES","declaration":""},
          {"id":12,"type":"Reserva Forestal","name":"El Caura","state":"Bolívar","latitude":6.2610516,"longitude":"-64.6998297","area":5134000.00,"date":"23-1-1968","link":"28541\n25/01/1968","date":"1045            23/01/1968","in_charge_of":"MINAMB","declaration":""},
          {"id":13,"type":"Reserva Forestal","name":"San Pedro","state":"Bolivar","latitude":6.7898695,"longitude":"-63.1070292","area":757400.00,"date":"5-6-2008","link":"38946\n05/06/2008","date":"6.070\n14/05/2.008","in_charge_of":"MINAMB","declaration":"Creado por Resoluci6n No 332 del 25 de noviembre de 1.981 publicada en Gaceta Oficial No 32.362. Decretado ABRAE en La Ley de Bosque y Gestión Forestal.\ndel 26 de noviembre de 1.981"},
          {"id":14,"type":"Reserva Forestal","name":"Tumeremo - El Dorado","state":"Bolívar","latitude":7.2995926,"longitude":"-61.5716802","area":78993.00,"date":"6-6-2008","link":"38946\n05/06/2009","date":"6.070\n14/05/2.009","in_charge_of":"MINAMB","declaration":"Creado por Resolución No 67 del 14 de diciembre de 1.987 publicada en Gaceta Oficial No 33.866 de fecha 14 de diciembre de 1.987", "image":"MINERIA-ILEGAL Tumeremo 1.jpg"},
          {"id":15,"type":"Reserva Forestal","name":"Rio Parguaza","state":"Bolívar","latitude":6.1744476,"longitude":"-67.0583454","area":65700.00,"date":"7-6-2008","link":"38946\n05/06/2010","date":"6.070\n14/05/2.010","in_charge_of":"MINAMB","declaration":"Creado por Resolución No 99 del 30 de diciembre de 1.986, publicada en Gaceta Oficial No 33.629 del 2 de enero de 1.987."},
          {"id":17,"type":"Reserva Forestal","name":"La Paragua","state":"Bolívar","latitude":6.8789135,"longitude":"-63.3637335","area":782000.00,"date":"23-1-1968","link":"28541\n25/01/1968","date":"1046            23/01/1968","in_charge_of":"MINAMB","declaration":""},
          {"id":19,"type":"Reserva Nacional Hidraulica","name":"Río Icabaru","state":"Bolívar","latitude":4.2325001,"longitude":"-62.0915999","area":null,"date":"5-6-1972","link":"4548-E\n26/03/1993","date":"2.311\n05/06/1992","in_charge_of":"MINAMB","declaration":""},
          {"id":20,"type":"Mineria Ilegal","name":"Las Claritas","state":"Bolivar","latitude":6.1792755,"longitude":"-61.4318348291"},
          {"id":21,"type":"Mineria Ilegal","name":"Campamento minero Las Cristina","state":"Bolivar","latitude":6.194037,"longitude":"-61.469201"},
          {"id":22,"type":"Mineria Ilegal","name":"Río Icabarú","state":"Bolivar","latitude":4.7348759,"longitude":"-62.3029367"},
          {"id":23,"type":"Mineria Ilegal","name":"Sifontes","state":"Bolivar","latitude":7.003272,"longitude":"-61.242690"},
          {"id":24,"type":"Mineria Ilegal","name":"Roscio","state":"Bolivar","latitude":7.323120,"longitude":"-61.947690"},
          {"id":25,"state":"Bolivar","latitude":7.6207333333,"longitude":-66.4098722222,"type":"Arco Minero"},
          {"id":26,"state":"Bolivar","latitude":7.65935,"longitude":-66.3347027778,"type":"Arco Minero"},
          {"id":27,"state":"Bolivar","latitude":7.7030111111,"longitude":-65.9571527778,"type":"Arco Minero"},
          {"id":28,"state":"Bolivar","latitude":7.8811888889,"longitude":-65.3625638889,"type":"Arco Minero"},
          {"id":29,"state":"Bolivar","latitude":8.0062277778,"longitude":-64.1245638889,"type":"Arco Minero"},
          {"id":30,"state":"Bolivar","latitude":8.3855388889,"longitude":-62.6886722222,"type":"Arco Minero"},
          {"id":31,"state":"Bolivar","latitude":8.4551333333,"longitude":-62.616175,"type":"Arco Minero"},
          {"id":32,"state":"Bolivar","latitude":8.4526222222,"longitude":-62.6013888889,"type":"Arco Minero"},
          {"id":33,"state":"Bolivar","latitude":8.5129916667,"longitude":-62.4948027778,"type":"Arco Minero"},
          {"id":34,"state":"Bolivar","latitude":8.5225611111,"longitude":-62.4116527778,"type":"Arco Minero"},
          {"id":35,"state":"Bolivar","latitude":8.5618416667,"longitude":-62.3376444444,"type":"Arco Minero"},
          {"id":36,"state":"Bolivar","latitude":8.5892694444,"longitude":-62.2556916667,"type":"Arco Minero"},
          {"id":37,"state":"Bolivar","latitude":8.6358222222,"longitude":-62.2195472222,"type":"Arco Minero"},
          {"id":38,"state":"Bolivar","latitude":8.6913027778,"longitude":-62.1823944444,"type":"Arco Minero"},
          {"id":39,"state":"Bolivar","latitude":8.7013611111,"longitude":-62.1693194444,"type":"Arco Minero"},
          {"id":40,"state":"Bolivar","latitude":8.5365333333,"longitude":-62.0394916667,"type":"Arco Minero"},
          {"id":41,"state":"Bolivar","latitude":8.3137166667,"longitude":-62.1780916667,"type":"Arco Minero"},
          {"id":42,"state":"Bolivar","latitude":8.4473861111,"longitude":-61.6922722222,"type":"Arco Minero"},
          {"id":43,"state":"Bolivar","latitude":7.9894944444,"longitude":-61.2414777778,"type":"Arco Minero"},
          {"id":44,"state":"Bolivar","latitude":7.8975111111,"longitude":-60.6889222222,"type":"Arco Minero"},
          {"id":45,"state":"Bolivar","latitude":7.9586888889,"longitude":-60.6885805556,"type":"Arco Minero"},
          {"id":46,"state":"Bolivar","latitude":7.958175,"longitude":-60.5979222222,"type":"Arco Minero"},
          {"id":47,"state":"Bolivar","latitude":7.9954361111,"longitude":-60.5977027778,"type":"Arco Minero"},
          {"id":48,"state":"Bolivar","latitude":7.99635,"longitude":-60.3257083333,"type":"Arco Minero"},
          {"id":49,"state":"Bolivar","latitude":7.9045194444,"longitude":-60.326275,"type":"Arco Minero"},
          {"id":50,"state":"Bolivar","latitude":7.9057638889,"longitude":-60.2616861111,"type":"Arco Minero"},
          {"id":51,"state":"Bolivar","latitude":5.9153472222,"longitude":-61.37145,"type":"Arco Minero"},
          {"id":52,"state":"Bolivar","latitude":5.9277305556,"longitude":-61.38245,"type":"Arco Minero"},
          {"id":53,"state":"Bolivar","latitude":6.0366444444,"longitude":-61.3558,"type":"Arco Minero"},
          {"id":54,"state":"Bolivar","latitude":6.1160805556,"longitude":-61.3998972222,"type":"Arco Minero"},
          {"id":55,"state":"Bolivar","latitude":6.1155472222,"longitude":-61.4034944444,"type":"Arco Minero"},
          {"id":56,"state":"Bolivar","latitude":6.1189638889,"longitude":-61.5831972222,"type":"Arco Minero"},
          {"id":57,"state":"Bolivar","latitude":6.1377305556,"longitude":-61.5664777778,"type":"Arco Minero"},
          {"id":58,"state":"Bolivar","latitude":6.1716388889,"longitude":-62.1014444444,"type":"Arco Minero"},
          {"id":59,"state":"Bolivar","latitude":6.1718472222,"longitude":-62.1017333333,"type":"Arco Minero"},
          {"id":60,"state":"Bolivar","latitude":6.2270777778,"longitude":-62.1745444444,"type":"Arco Minero"},
          {"id":61,"state":"Bolivar","latitude":6.5267694444,"longitude":-62.4165972222,"type":"Arco Minero"},
          {"id":62,"state":"Bolivar","latitude":6.5305333333,"longitude":-62.8274,"type":"Arco Minero"},
          {"id":63,"state":"Bolivar","latitude":6.7379222222,"longitude":-62.8008972222,"type":"Arco Minero"},
          {"id":64,"state":"Bolivar","latitude":6.7732305556,"longitude":-62.8040583333,"type":"Arco Minero"},
          {"id":65,"state":"Bolivar","latitude":6.8304722222,"longitude":-62.7606583333,"type":"Arco Minero"},
          {"id":66,"state":"Bolivar","latitude":6.9116083333,"longitude":-62.7677777778,"type":"Arco Minero"},
          {"id":67,"state":"Bolivar","latitude":6.9485138889,"longitude":-62.7826416667,"type":"Arco Minero"},
          {"id":68,"state":"Bolivar","latitude":6.9252722222,"longitude":-62.9142555556,"type":"Arco Minero"},
          {"id":69,"state":"Bolivar","latitude":6.9232083333,"longitude":-62.9677805556,"type":"Arco Minero"},
          {"id":70,"state":"Bolivar","latitude":6.9147611111,"longitude":-63.3174277778,"type":"Arco Minero"},
          {"id":71,"state":"Bolivar","latitude":6.9629388889,"longitude":-63.3445388889,"type":"Arco Minero"},
          {"id":72,"state":"Bolivar","latitude":6.8587111111,"longitude":-63.8841666667,"type":"Arco Minero"},
          {"id":73,"state":"Bolivar","latitude":6.8947694444,"longitude":-63.8904666667,"type":"Arco Minero"},
          {"id":74,"state":"Bolivar","latitude":7.0453833333,"longitude":-64.0651027778,"type":"Arco Minero"},
          {"id":75,"state":"Bolivar","latitude":7.0823277778,"longitude":-64.0376,"type":"Arco Minero"},
          {"id":76,"state":"Bolivar","latitude":7.0862888889,"longitude":-64.3199444444,"type":"Arco Minero"},
          {"id":77,"state":"Bolivar","latitude":7.1036944444,"longitude":-64.3765055556,"type":"Arco Minero"},
          {"id":78,"state":"Bolivar","latitude":7.1133222222,"longitude":-64.3867361111,"type":"Arco Minero"},
          {"id":79,"state":"Bolivar","latitude":6.8790388889,"longitude":-64.7939944444,"type":"Arco Minero"},
          {"id":80,"state":"Bolivar","latitude":6.879475,"longitude":-64.8031333333,"type":"Arco Minero"},
          {"id":81,"state":"Bolivar","latitude":7.1915083333,"longitude":-65.1238305556,"type":"Arco Minero"},
          {"id":82,"state":"Bolivar","latitude":7.0499527778,"longitude":-65.4810944444,"type":"Arco Minero"},
          {"id":83,"state":"Bolivar","latitude":7.0756388889,"longitude":-65.5015861111,"type":"Arco Minero"},
          {"id":84,"state":"Bolivar","latitude":7.11385,"longitude":-65.63475,"type":"Arco Minero"},
          {"id":85,"state":"Bolivar","latitude":6.8903222222,"longitude":-65.6309722222,"type":"Arco Minero"},
          {"id":86,"state":"Bolivar","latitude":6.4290916667,"longitude":-65.6614972222,"type":"Arco Minero"},
          {"id":87,"state":"Bolivar","latitude":6.2292555556,"longitude":-65.6619277778,"type":"Arco Minero"},
          {"id":88,"state":"Bolivar","latitude":6.2347305556,"longitude":-66.0241722222,"type":"Arco Minero"},
          {"id":89,"state":"Bolivar","latitude":6.2415194444,"longitude":-66.0232861111,"type":"Arco Minero"},
          {"id":90,"state":"Bolivar","latitude":6.244425,"longitude":-66.10855,"type":"Arco Minero"},
          {"id":91,"state":"Bolivar","latitude":6.2513083333,"longitude":-66.1177944444,"type":"Arco Minero"},
          {"id":92,"state":"Bolivar","latitude":5.9722111111,"longitude":-66.2858055556,"type":"Arco Minero"},
          {"id":93,"state":"Bolivar","latitude":6.0057833333,"longitude":-66.3264388889,"type":"Arco Minero"},
          {"id":94,"state":"Bolivar","latitude":6.0073555556,"longitude":-66.4834194444,"type":"Arco Minero"},
          {"id":95,"state":"Bolivar","latitude":6.0089555556,"longitude":-66.4826694444,"type":"Arco Minero"},
          {"id":96,"state":"Bolivar","latitude":6.0020888889,"longitude":-66.4847055556,"type":"Arco Minero"},
          {"id":97,"state":"Bolivar","latitude":5.1012611111,"longitude":-66.5085638889,"type":"Arco Minero"},
          {"id":98,"state":"Bolivar","latitude":6.01095,"longitude":-66.9950194444,"type":"Arco Minero"},
          {"id":99,"state":"Bolivar","latitude":6.029875,"longitude":-67.1197,"type":"Arco Minero"},
          {"id":100,"state":"Bolivar","latitude":6.0497166667,"longitude":-67.1260527778,"type":"Arco Minero"},
          {"id":101,"state":"Bolivar","latitude":6.0666833333,"longitude":-67.1433,"type":"Arco Minero"},
          {"id":102,"state":"Bolivar","latitude":6.1116138889,"longitude":-67.243525,"type":"Arco Minero"},
          {"id":103,"state":"Bolivar","latitude":6.1140861111,"longitude":-67.2839416667,"type":"Arco Minero"},
          {"id":104,"state":"Bolivar","latitude":6.1485694444,"longitude":-67.3080638889,"type":"Arco Minero"},
          {"id":105,"state":"Bolivar","latitude":6.1889611111,"longitude":-67.4353083333,"type":"Arco Minero"},
          {"id":106,"state":"Bolivar","latitude":6.1976111111,"longitude":-67.4394666667,"type":"Arco Minero"},
          {"id":107,"state":"Bolivar","latitude":6.6296333333,"longitude":-67.1160194444,"type":"Arco Minero"},
          {"id":108,"state":"Bolivar","latitude":6.6309611111,"longitude":-67.1250027778,"type":"Arco Minero"}
        ]

        vm.array_of_number = 
        [
          {"id":1,"name":"Reservas y Proteción de Presas y Embalses","color":"#d1ff66"},
          {"id":2,"name":"Reservas Nacionales Hidráulicas","color":"#70f3ec"},
          {"id":3,"name":"Áreas Criticas con prioridad de tratamiento","color":"#6edf78"},
          {"id":4,"name":"Zonas Protectoras","color":"#b4996e"},
          {"id":5,"name":"Refugios de Fauna Silvestre","color":"#e8cd68"},
          {"id":6,"name":"Reservas de Fauna Silvestre","color":"#afa066"},
          {"id":7,"name":"Santuarios de Fauna Silvestre","color":"#ff1ce8"},
          {"id":8,"name":"Áreas de protección y Recuperación Ambiental","color":"#a3ec72"},
          {"id":9,"name":"Zonas de Aprovechamiento Agrícola","color":"#9bff82"},
          {"id":10,"name":"Zonas de Seguridad Fronteriza","color":"#ffab66"},
          {"id":11,"name":"Reservas Forestales","color":"#e0b96c"},
          {"id":12,"name":"Reservas de Biosfera","color":"#b96666"},
          {"id":13,"name":" Áreas Rurales de Desarrollo Integral","color":"#84e184"},
          {"id":14,"name":"Monumentos Naturales","color":"#a8ab68"},
          {"id":15,"name":" Parques Nacionales","color":"#688e69"},
          {"id":16,"name":"Áreas Protectoras de Obras Públicas","color":"#968867"},
          {"id":17,"name":"Áreas de Vocación Forestal","color":"#68fa8a"},
          {"id":19,"name":"Todos","color":"black"}
        ];

        vm.translate = function ()
          {
            if ( vm.x )
              {
                vm.language = Texts.español;
              }
            else
              {
                vm.language = Texts.english;
              }
            return vm.changeValue();
          }

        vm.changeValue = function()
          {
            vm.x = !vm.x
          }

        vm.toggleModal = function() 
        {
           vm.showModal = !vm.showModal;

           if ( vm.showModal && $( window ).width() < 768 )
            { 
              $( '#boton' ).click();
            }
            
        }

        vm.showMap = function(id)
        {
          vm.image_map = "img/maps_venezuela/"+id+".png";
        }


      var createMarkers = function (){
        //console.log('llega gmaps');
            var map = new GMaps({
                div: '#google-map',
                zoom :7,
                lat: vm.array_of_point[0].latitude,
                lng: vm.array_of_point[0].longitude,
                panControl : false,
                streetViewControl : false,
                mapTypeControl: false,
                overviewMapControl: false
            });

            angular.forEach(vm.array_of_point, function(value) {
              
              var img = " ";

              if (value.type == "Monumento Natural") {
                img = 'img/map_pin_brown.png';
              }else if (value.type == "Reserva Forestal") {
                img = 'img/map_pin_green.png';
              }else if (value.type == "Mineria Ilegal") {
                img = 'img/map_pin_red.png';
              }else if (value.type == "Reserva Nacional Hidraulica") {
                img = 'img/map_pin_blue.png';
              }else if (value.type == "Arco Minero") {
                img = 'img/map_pin_purple.png';
              }

              map.addMarker({
                  icon :img,
                  lat: value.latitude,
                  lng: value.longitude,
                  title: value.name,
                  infoWindow: {
                              content: '<div><h3>' + value.name + '</h3></div><div style="width:30px;height:30px;background-image:url(img/'+value.image+');"><div class="infoWindowContent">' + value.type + " " + "Fecha de creacion:"+" "+ value.date +
                              '<ul class="list-unstyled">'
                          }
              });
            });

        }

        createMarkers();
      }
  ] );