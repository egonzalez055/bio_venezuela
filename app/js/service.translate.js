'use strict';

index_controller.service( 'Texts' , function( ) 
  {

     this.español = {
     philosophy: "Zonas protegidas por el Estado dentro del Arco Minero del Orinoco y su impacto.",
     text1: "Quienes Somos",
     text2: "consecuencias de la minería",
     text4: "Zonas ABRAE",
     text3: "Servicios",
     text5: "Mapa",
     idioma: "Español",
     about: "Quiénes somos",
     text_about: "Somos una plataforma de visiualización y monitoreo de las Zonas ABRAE en el Arco Minero del Orinoco.",
     mission: "Misión",
     text_mission: "Proporcionarle a nuestros clientes la mayor tranquilidad y estabilidad a la hora de dar respuesta a sus requerimientos legales y contables. Brindándoles nuestros servicios caracterizados por la confianza,responsabilidad, solidez y eficiencia. ",
     vision: "Visión",
     text_vision: "Ser el Escritorio Jurídico Contable referencia a la hora de buscar la solución y atención a los requerimientos legales y contables de empresas y particulares. Manteniendo nuestra presencia en el territorio venezolano y conservando el respeto y la confianza invertida en nosotros.",
     legal_area: "Área Legal",
     service1: "Asesoría legal.",
     service2: "Redacción y Revisión de documentos.",
     service3: "Asistencia y Representación judicial.",
     service4: "Representación ante organismos públicos del Estado.",
     service5: "Informe periódico contentivo de las gestiones llevadas a cabo por nuestros profesionales con motivo del avance de casos.",
     accounting_area: "Área Contable",
     service6: "Balance y Certificación de Ingresos.",
     service7: "Asistencia en temas relacionados al pago del Impuesto Sobre la Renta (ISLR), Impuesto al Valor Agregado (IVA), y otros.",
     common_law: "Derecho Civil",
     common_law_text: "Redacción de todo tipo de documentos enmarcados en materia de personas, bienes, sucesiones, familia. Tales como carta de soltería, acuerdo de unión estable de hecho, declaración jurada de no poseer vivienda, cesión de derechos, curatela justificativo de únicos y universales herederos, poder o mandato, autorizaciones, divorcio 185-A, separación de cuerpos, capitulaciones matrimoniales, título supletorio, opción compra-venta, venta de bienes muebles e inmuebles, arrendamiento de viviendas, entre otros. Además, le asistimos en procesos judiciales  tales como: particiones hereditarias, prescripciones adquisitivas, impugnación de actas de asamblea de condominio, liquidación de comunidad conyugal, acciones merodeclarativas de derechos y, en general, cualquier otra acción de esta naturaleza contemplada en la legislación venezolana.",
     minor_law: "Derecho del Niño, Niña y Adolescente",
     minor_law_text: "Asistencia y asesoría en relación a la fijación de la Obligación de manutención y del Régimen de Convivencia familiar. Representación ante organismos públicos en temas de la materia. Redacción de autorizaciones para viajes nacionales e internacionales y para residencias. Asistencia en asuntos de naturaleza contenciosa relacionados con la patria potestad, responsabilidad de crianza, régimen de convivencia, divorcio, nulidad de matrimonio y separación de cuerpos cuando hay menores de edad en común entre las partes, liquidación y partición de la comunidad conyugal o de uniones estables de hecho cuando hay menores de edad en común entre las partes.",
     business_law: "Derecho Mercantil",
     business_law_text: "Constitución de compañías anónimas y sociedades de responsabilidad limitada; firma personal; contratos de naturaleza mercantil en general; actas de asamblea de accionistas ordinaria y extraordinaria. Asesoría integral en materia de constitución de cooperativas.",
     property_law: "Derecho Inmobiliario",
     property_law_text: "Asesoría en casos relacionados al arrendamiento de viviendas. Defensa y asistencia en los trámites ante la Superintendencia Nacional de Arrendamientos de Vivienda (SUNAVI), tales como consignación y regulación de canon de arrendamiento, conciliaciones entre arrendador y arrendatario, registro. Inscripciones ante el SAVIL.",
     intellectual_property: "Propiedad Intelectual",
     intellectual_property_text: "Asesoría y asistencia en temas relacionados con el registro de marcas (producto o servicio), nombre comercial, denominación comercial, lema comercial, patentes de invención ante el Servicio Autónomo de Propiedad Intelectual (SAPI).",
     probate_law: "Derecho Sucesoral",
     probate_law_text: "Asistencia en trámites de declaración sucesoral ante el SENIAT.",
     footer: "Todos los derechos reservados.",
     contact_us: "Contactanos: Kachiriapp@gmail.com",
     client_email: "Correo electronico: client@gmail.com",
     client_phone_number: "Numero Telefonico: 00000000000",
     modal_message: "Gracias por preferirnos."
     };

     this.english = {
     philosophy: "Zonas protegidas por el Estado dentro del Arco Minero del Orinoco y su impacto.",
     text1: "About Us",
     text2: "Consequences of mining",
     text4: "ABRAE zones",
     text3: "Services",
     text5: "Map",
     idioma: "English",
     about: "About",
     text_about: "Somos una plataforma de visiualización y monitoreo de las Zonas ABRAE en el Arco Minero del Orinoco.",
     mission: "Mission",
     text_mission: "Give our clients  stability and calmness at the time to reply to your legal and accounting  requirements. Offer our services characterized for the trust, responsibility, strength and efficiency.",
     vision: "Vision",
     text_vision: "Be The most recommended legal and accounting office at the time of looking for a solution and complete care to legal and accounting requirements of business and Individuals keeping our presence in the Venezuelan territory and remaining the respect and trust Invested on us.",
     legal_area: "Legal Area",
     service1: "legal  consulting.",
     service2: "composition and reviewing documents.",
     service3: "legal representation and assistance.",
     service4: "Behalf before government institution.",
     service5: "Constant information about the work featured by our professionals in your case.",
     accounting_area: "Accounting Area",
     service6: "Certified Financial Statement.",
     service7: "Assistance in tax payment, IRS, among others.",
     common_law: "Common Law",
     common_law_text: "Composition of every kind of documents inside the subject-matter of; civil rights, property,  inheritance and family. Such as; Single Status Affidavit, stable de facto union, real estate affidavit ownership,  cession of rights, unique and universal heirs, power of attorney, authorization, divorce 185-A, legal separation, Title by Adverse Possession,  call or put option definition, sell of goods, residential lease agreement, among others. Besides we will assistance you in judicial proceedings such as: hereditary property of partitions, acquisitive prescription, condo owner rights and responsibilities, distribution of conjugal property, action of recognition of rights and any other action of this nature stated in the Venezuelan legislation.",
     minor_law: "Minor's Law",
     minor_law_text: "Assistance and  consultancy in relation to the statement of maintenance and familiar coexistence regime. Behalf before government institutions in the subject-matter. Composition of authorizations for national and international trips and residency. Assistance in subject of the nature of custody, parental responsibility,  coexistence regime, divorce,  nullity of marriage and legal separation when there is common minors, liquidation,  distribution and liquidation of conjugal property when there is minors.",
     business_law: "Business Law",
     business_law_text: "Constitution of companies and limited liability company;  personal signature; business contracts  in general;  minutes of shareholders meeting ordinary and extraordinary.  Complete consultancy in subject-matter of constitution of business coperatives.",
     property_law: "Property Law",
     property_law_text: "Consultancy in cases related to renting. Protection and assistance in procedures of the National Superintendence of Housing Renting (SUNAVI, in Spanish), such as consignment and regulation of renting canon , conciliation between lessee and owner, register. Enrollment at the SAVIL.",
     intellectual_property: "Intellectual Property",
     intellectual_property_text: "Consultancy and assistance in subject-matter related to trademark (product or service), commercial name, commercial denomination, commercial slogan, patent invention to the Autonomous Service of Intellectual Property (SAPI in Spanish).",
     probate_law: "Probate Law",
     probate_law_text: "Assistance in procedures succession statement to the SENIAT.",
     footer: "All rights reserved.",
     contact_us: "Contact us: Kachiriapp@gmail.com",
     client_email: "Email: client@gmail.com",
     client_phone_number: "Phone Number: 00000000000",
     modal_message: "Thanks for thinking on us."
     };

  } );